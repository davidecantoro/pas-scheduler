import pandas as pd
from pymongo import MongoClient
import logging
import os
from datetime import datetime, date

def setup_logging():
    # Create a logger object
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Set the log format
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Create a log handler to print logs on stdout
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)

    # Add the log manager to the logger 
    logger.addHandler(stdout_handler)

    return logger

logger = setup_logging()

# MongoDB settings and connection
mongo_url = 'mongodb://172.18.0.2:27017/'
client = MongoClient(mongo_url)
database = 'mydatabase'
name_citizens = 'citizens'
name_payment = 'payment'
db = client[database]

# Retrieve data from MongoDB
collection_citizens = db[name_citizens]
citizens = collection_citizens.find()
data_c = list(citizens)
df_citizens = pd.DataFrame(data_c)

# Create payments and insert into Payment collection
for index, row in df_citizens.iterrows():
    #citizen_id = row['_id']
    payee = row['citizen']
    amount = 100.00
    start_period = datetime(date.today().year, 1, 1)
    end_period = datetime(date.today().year, 12, 31)
    paid = None
    status = 'not paid'
    
    payment_dic = {
        'payee': payee,
        'amount': amount,
        'startPeriod': start_period,
        'endPeriod': end_period,
        'paid': paid,
        'status': status
    }
    
    collection_payment = db[name_payment]
    result = collection_payment.insert_one(payment_dic)
    
    if result.inserted_id:
        logger.info(f"Payment created: Citizen : {payee}, amount : {amount}, from date {start_period} to date {end_period}")

client.close()
