#!/bin/bash

#####################################################################################################
#                                                                                                   #
#  This program waits for the amount of time passed as parameters before starting the containers.   #
#  Usage Ex:     ./year_payment_single_run.sh DAYS HOURS MINUTE                                     #
#                                                                                                   #
#####################################################################################################

path_year_payment = '/home/ubuntu/scheduler/year_payment'



days=$1
hours=$2
minutes=$3
total_seconds=$((days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60))

# sleep until date
echo "Waiting for $days days, $hours hours, and $minutes minutes..."
sleep $total_seconds

 

echo "Calculating payments at $current_time on $current_date"
cd path_year_payment
docker-compose up --build
    

