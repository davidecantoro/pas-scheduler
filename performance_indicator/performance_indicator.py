import pandas as pd
import statistics as st
from pymongo import MongoClient
import logging
import os

def setup_logging():
    # Create a logger object
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Set the log format
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Create a log handler to print logs on stdout
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)

    # Add the log manager to the logger 
    logger.addHandler(stdout_handler)

    return logger

logger = setup_logging()


# get max value of fillingLevel by day  for each bin
def get_all_filling_level_max(dataset, name_id, filling_level_name):
  # Group dataset
  df = dataset.groupby([name_id, dataset['timestamp'].dt.date])[filling_level_name].max()
  df = df.reset_index()

  fillingLevel_name = df[name_id].unique()      # Get unique bin IDs
  fillingLevelMax = [df[df[name_id] == i][filling_level_name].tolist() for i in fillingLevel_name]

  return fillingLevel_name, fillingLevelMax

# calculate median of fillingLevel for each bins
def get_median_bin(fillingLevel_name,fillingLevelMax):
  median = [round(st.median(fill_levels), 4) for fill_levels in fillingLevelMax]
  return median

# get median value for a given bin
def get_median_by_name(name_bin, df_median):
  median_value = df_median['median'][df_median.bin == name_bin]
  return float(median_value.iloc[0])

#calculate performance indicator and update citizens dataframe
def update_performance_indicator(dataframe,median):
  for iterator, row in dataframe.iterrows():
    bin_names = row.iloc[5:].tolist()[0]
    bin_values = [get_median_by_name(bin_name, median) for bin_name in bin_names]
    total_bin_values = sum(bin_values)
    index_performance = bin_values[0] / total_bin_values if total_bin_values > 0 else 0.0
    dataframe.at[iterator, 'performanceIndicator'] = 1.0 - index_performance
  return dataframe



# MongoDB settings e connection
mongo_url = 'mongodb://172.18.0.2:27017/'
client = MongoClient(mongo_url)
database = 'mydatabase'
name_measure = 'ts_measurements'
name_citizens = 'citizens'
db = client[database]

# Retrieve data from MongoDB
collection_measure = db[name_measure]
collection_citizens = db[name_citizens]

measures = collection_measure.find()
citizens = collection_citizens.find()

# create dataframe
data_m = list(measures)
df_measurements = pd.DataFrame(data_m)

data_c = list(citizens)
df_citizens = pd.DataFrame(data_c)

# starting script

#get median for each element
list_filling_name, list_filling_level = get_all_filling_level_max(df_measurements, 'idBin', 'fillingLevel')
median_bin = get_median_bin(list_filling_name,list_filling_level)
df_median=pd.DataFrame(list(zip(list_filling_name,median_bin)),columns =['bin', 'median'])

#calculate performance indicator and dataset
logger.info('Data taken from mongo')
logger.info('\n\n' + df_citizens[['citizen', 'performanceIndicator', 'streetAddress', 'locality']].to_string(index=False) + '\n\n')
logger.info('Calculating new performance indicator...')
logger.info('New performance indicator calculated')
df_citizens = update_performance_indicator(df_citizens, df_median)
logger.info('\n\n' + df_citizens[['citizen', 'performanceIndicator', 'streetAddress', 'locality']].to_string(index=False))


# update collections in MongoDB
for idx, row in df_citizens.iterrows():
    new_performance_indicator = row['performanceIndicator']
    citizen_id = row['_id']

    collection_citizens.update_one(
        {"_id": citizen_id},
        {"$set": {"performanceIndicator": new_performance_indicator}}
    )

client.close()