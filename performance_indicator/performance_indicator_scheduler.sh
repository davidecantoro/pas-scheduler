#!/bin/bash

#####################################################################################################
#                                                                                                   #
#   This script starts every day at the desired time.                                               #
#   This script makes use of cron to schedule the execution of the script                           #
#   Initial configurations:                                                                         #
#       Enter a new instance on cron using:                                                         #
#           crontab -e                                                                              #
#           0 9 * * * /PATH/performance_indicator_scheduler.sh                                      #
#       PATH is the path where the script is located.                                               #
#                                                                                                   #
#####################################################################################################

path_performance_indicator = '/home/ubuntu/scheduler/performance_indicator'

current_date=$(date +"%Y-%m-%d")
current_time=$(date +"%H:%M:%S")
current_hour=$(date +"%H")

# Set the target hour for starting the container
target_hour="09"



# Check if the current hour matches the target start hour
if [ "$current_hour" == "$target_hour" ]; then

    echo "New performance indicator calculator started at $current_time on $current_date"
    cd path_performance_indicator
    docker-compose up --build
    
else
    echo "It's not yet the desired start hour."
fi
