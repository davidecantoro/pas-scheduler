#!/bin/bash

#####################################################################################################
#                                                                                                   #
#  This program waits for the amount of time passed as parameters before starting the containers.   #
#  Usage Ex:     ./performance_indicator_single_run.sh DAYS HOURS MINUTE                            #
#                                                                                                   #
#####################################################################################################

path_performance_indicator = '/home/ubuntu/scheduler/performance_indicator'



days=$1
hours=$2
minutes=$3
total_seconds=$((days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60))

# sleep until date
echo "Waiting for $days days, $hours hours, and $minutes minutes..."
sleep $total_seconds

 

echo "New performance indicator calculator started at $current_time on $current_date"
cd path_performance_indicator
docker-compose up --build
    

